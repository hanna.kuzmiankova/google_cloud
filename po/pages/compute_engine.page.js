const  ComputeEngineComponents = require("../components/compute_engine.component");
class ComputeEnginePage {
    constructor(){
        this.computeEngineOptions = new ComputeEngineComponents();
    }
};
module.exports = new ComputeEnginePage();