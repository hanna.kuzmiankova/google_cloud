class GoogleCloudLandingPage {
    // constructor(){
    //     this.optionalSettings = new OptionalPasteSettingsComponent();
    // }
async open() {
    await browser.url('https://cloud.google.com/');
};
get iconSearch(){
   return $('.YSM5S');
}
get fieldSearch(){
    return $('#i4');
}
get enterSearchResult(){
    return $('i[aria-label="Search"]');
}

async inputSearchValue(text){
    (await this.iconSearch).waitForDisplayed();
    await this.iconSearch.click();
    await this.fieldSearch.setValue(text);
    await this.enterSearchResult.click();
  }

}
module.exports = new GoogleCloudLandingPage();