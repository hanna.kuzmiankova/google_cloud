class ComputeEngineOptions{
get setNumberOfInstances(){
    return $('#c11');
}
get machineType(){   
  return $('//span[text()="Machine type"]/ancestor::div[contains(@class, "O1htCb-H9tDt")]');  
}
get machineTypeOptions(){   
  return $('//span[text()="n1-standard-8"]/ancestor::li[contains(@class, "MCs1Pd")]');
}
get addGPUs(){   
   return $('button[aria-label="Add GPUs"]');    
}
get gpuModelwaitForDisplayed(){
  return $('//span[text()="GPU Model"]/ancestor::div[contains(@class, "VfPpkd-TkwUic")]');
}
get gpuModel(){
  return $('//span[text()="GPU Model"]/ancestor::div[contains(@class, "MSEcuf")]');
}

get gpuModelTeslaV100(){
  return $('//span[text()="NVIDIA Tesla V100"]/ancestor::li[contains(@class, "MCs1Pd")]'); 
}
get localSSD(){
  return $('//span[text()="Local SSD"]/ancestor::div[contains(@class, "VfPpkd-O1htCb")]');
}
get localSSD2x375GB(){
  return $('//span[text()="2x375 GB"]/ancestor::li[contains(@class, "MCs1Pd")]');
}
get committedUseDiscountOptions(){
  return $('//label[text()="1 year"]/ancestor::div[contains(@class, "e2WL2b")]');
}
get estimatedCostInHeader(){
  return $('//span[text()="/ month"]/preceding-sibling::span');
}
get finalEstimatedCost(){
  return $('//div[text()="Estimated cost"]/ancestor::div[contains(@class, "wFCpDb")]//label');
} 
get shareButton(){
  return $('//span[text()="Share"]');
}
get openEstimateSummary(){
  return $('//a[text()="Open estimate summary"]');
}

async chooseMachineTypeOptions(){
  await this.machineType.click();
  await this.machineTypeOptions.waitForDisplayed;
  await this.machineTypeOptions.click();
}
async choosegpuModel(){
  await this.gpuModel.click();
  await this.gpuModelTeslaV100.waitForDisplayed;
  await this.gpuModelTeslaV100.click();
}
async chooselocalSSD2x375GB(){
  await this.localSSD.click();
  await this.localSSD2x375GB.waitForDisplayed;
  await this.localSSD2x375GB.click();
}
async inputNumberOfInstances(number){
    await this.setNumberOfInstances.setValue(number);
  }
}
module.exports = ComputeEngineOptions;