const GoogleCloudLandingPage = require("../../po/pages/google_cloud_landing.page.js");
const GoogleCloudSearchResultPage = require("../../po/pages/google_cloud_search_result.page.js");
const GoogleCloudPricingCalculatorPage = require("../../po/pages/google_cloud_pricing_calculator.page.js");
const ComputeEnginePage = require("../../po/pages/compute_engine.page.js");
const CostEstimateSummaryPage = require("../../po/pages/cost_estimate_summary.page.js");

describe("Google Cloud Platform Pricing Calculator", () => {
  const estimateMenu = GoogleCloudPricingCalculatorPage.estimateComponents;
  const computeEngineOptions = ComputeEnginePage.computeEngineOptions;
  const valueForSearchField = "Google Cloud Platform Pricing Calculator";
  const numberOfinstance = 4;

  before(async () => {
    await GoogleCloudLandingPage.open();
    await GoogleCloudLandingPage.inputSearchValue(valueForSearchField);
    await GoogleCloudSearchResultPage.firstSearchResult.waitForDisplayed();
    await GoogleCloudSearchResultPage.firstSearchResult.click();
    await GoogleCloudPricingCalculatorPage.addToEstimateButton.waitForDisplayed();
    await GoogleCloudPricingCalculatorPage.addToEstimateButton.click();
    await estimateMenu.computeEngine.waitForDisplayed();
    await estimateMenu.computeEngine.click();
    await computeEngineOptions.setNumberOfInstances.waitForDisplayed();
    await computeEngineOptions.inputNumberOfInstances(numberOfinstance);
    await computeEngineOptions.chooseMachineTypeOptions();
    await computeEngineOptions.addGPUs.click();
    await computeEngineOptions.gpuModelwaitForDisplayed.waitForDisplayed();
    await computeEngineOptions.choosegpuModel();
    await computeEngineOptions.chooselocalSSD2x375GB();
    await computeEngineOptions.committedUseDiscountOptions.click();
    // give time to calculate the final estimated summary;
    await browser.pause(1000);
  });

  //8. Check the price is calculated in the right section of the calculator. There is a line “Total Estimated Cost: USD ${amount} per 1 month”
  it("The total estimated cost matches the amount of the estimated cost in the header '@smoke'", async () => {
    const computeEngineCost = await computeEngineOptions.estimatedCostInHeader;
    const estimatedCost = await computeEngineOptions.finalEstimatedCost;
    const estimatedCostText = await estimatedCost.getText();
    await expect(computeEngineCost).toHaveText(estimatedCostText);
  });
  describe("Total cost result page", function () {
    before(async function () {
      await computeEngineOptions.shareButton.click();
      await computeEngineOptions.openEstimateSummary.waitForDisplayed();
      await computeEngineOptions.openEstimateSummary.click();
      await browser.switchWindow("Google Cloud Estimate Summary");
      await CostEstimateSummaryPage.numberOfInstances.waitForDisplayed();
    });
    //10.verify that the 'Cost Estimate Summary' matches with filled values in Step 6.
    it("The number of instances value should be equal the value '4' '@smoke'", async () => {
      const numberOfinstance = await CostEstimateSummaryPage.numberOfInstances;
      await expect(numberOfinstance).toHaveText(expect.stringContaining("4"));
    });
    it("The machine type value should be equal the value 'n1-standard-8, vCPUs: 8, RAM: 30 GB'", async () => {
      const machineType = await CostEstimateSummaryPage.machineType;
      await expect(machineType).toHaveText(
        expect.stringContaining("n1-standard-8")
      );
    });
    it("The 'Add GPUs' value should be equal the value 'true'", async () => {
      const addGPUS = await CostEstimateSummaryPage.addGPUs;
      await expect(addGPUS).toHaveText("true");
    });
    it("The 'GPU type' value should be equal the value 'NVIDIA Tesla V100'", async () => {
      const gpuModel = await CostEstimateSummaryPage.gpuModel;
      await expect(gpuModel).toHaveText(
        expect.stringContaining("NVIDIA Tesla V100")
      );
    });
    it("The 'Local SSD' value should be equal the value '2x375 GB'", async () => {
      const localSSD2x375GB = await CostEstimateSummaryPage.localSSD2x375GB;
      await expect(localSSD2x375GB).toHaveText(
        expect.stringContaining("2x375 GB")
      );
    });
    it("The 'Committed use discount options' value should be equal the value '1 year'", async () => {
      const committedUseDiscountOptions =
        await CostEstimateSummaryPage.committedUseDiscountOptions;
      await expect(committedUseDiscountOptions).toHaveText(
        expect.stringContaining("1 year")
      );
    });
  });
});
